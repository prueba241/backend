﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
   public class Client
    {
        public int IdClient { get; set; }

        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(50, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Cedula { get; set; }

        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Phone { get; set; }

        public bool Active { get; set; }


    }
}
