﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class SaleDetail : Sale
    {
        public string NameProduct { get; set; }

        public string NameClient { get; set; }
    }
}
