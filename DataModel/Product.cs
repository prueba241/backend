﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Product
    {
        public int IdPorduct { get; set; }

        [Required(ErrorMessage = "El campo {0} es Requerido")]
        [MaxLength(100, ErrorMessage = "El campo {0} Debe tener maximo {1} caracteres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "El campo {0} es Requerido")]
        public decimal UnitValue { get; set; }

        public bool Active { get; set; }

    }
}
