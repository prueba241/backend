﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Sale
    {
        public int IdSale { get; set; }

        public int IdProduct { get; set; }

        public int IdClient { get; set; }

        public decimal Amount { get; set; }

        public decimal UnitValue { get; set; }

        public decimal TotalValue { get; set; }

    }
}
