﻿using Business.Interface;
using DataAccess;
using DataAccess.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ProductBusiness : ProductInterface
    {
        ProductDataInterface productData = new ProductData();

        public void CreateUpdateProduct(Product product)
        {
            try
            {
                if (string.IsNullOrEmpty(product.Name))
                    throw new Exception("Parametro Nombre Obligatorio");

                if (product.UnitValue == 0)
                    throw new Exception("Parametro Valor Unitario es Obligatorio");

                productData.CreateAndUpdateProduct(product);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void DeleteProduct(int IdPorduct)
        {
            try
            {
                productData.DeleteProduct(IdPorduct);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Product GetProductId(int IdPorduct)
        {
            try
            {
                var product = productData.GetProductId(IdPorduct);
                return product;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Product> ListProduct()
        {
            try
            {
                var products = productData.ListProduct();
                return products;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
