﻿using Business.Interface;
using DataAccess;
using DataAccess.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class ClientBusiness : ClientInterface
    {
        ClientDataInterface clientData = new ClientData();

        public void CreateAndUpdateClient(Client Client)
        {
            try
            {
                if (string.IsNullOrEmpty(Client.Cedula))
                    throw new Exception("Parametro Nombre Obligatorio");

                if (string.IsNullOrEmpty(Client.Name))
                    throw new Exception("Parametro Nombre Obligatorio");

                if (string.IsNullOrEmpty(Client.LastName))
                    throw new Exception("Parametro Nombre Obligatorio");

                if (string.IsNullOrEmpty(Client.Phone))
                    throw new Exception("Parametro Nombre Obligatorio");


                clientData.CreateAndUpdateClient(Client);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void DeleteClient(int IdPorduct)
        {
            try
            {
                clientData.DeleteClient(IdPorduct);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Client GetClientId(int IdClient)
        {
            try
            {
                var client = clientData.GetClientId(IdClient);
                return client;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<Client> ListClient()
        {
            try
            {
                var Clients = clientData.ListClient();
                return Clients;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
