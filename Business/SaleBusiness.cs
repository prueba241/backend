﻿using Business.Interface;
using DataAccess;
using DataAccess.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class SaleBusiness : SaleInterface
    {
        SaleDataInterface saleData = new SaleData();

        public void CreateAndUpdateSale(Sale sale)
        {
            try
            {
                saleData.CreateAndUpdateSale(sale);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CreateSaleList(List<Sale> sales)
        {
            try
            {
                foreach (var item in sales)
                {
                    saleData.CreateAndUpdateSale(item);
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<SaleDetail> ListSales()
        {
            try
            {
                var sales = saleData.ListSales();
                return sales;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
