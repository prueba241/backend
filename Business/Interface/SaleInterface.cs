﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interface
{
    public interface SaleInterface
    {
        /// <summary>
        /// Método para crear una venta
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        ///<param name="Sale"></param>
        void CreateAndUpdateSale(Sale sale);


        /// <summary>
        /// Método para listar ventas
        /// </summary>
        /// <returns>
        ///  List<Product> 
        /// </returns>
        List<SaleDetail> ListSales();


        /// <summary>
        /// Método para crear una venta
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        ///<param name="Sale"></param>
        void CreateSaleList(List<Sale> sale);
    }
}
