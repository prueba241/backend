﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interface
{
    public interface ClientInterface
    {
        /// <summary>
        /// Método para crear un nuevo cliente
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        ///<param name="Client"></param>
        void CreateAndUpdateClient(Client Client);


        /// <summary>
        /// Método para ELIMINAR UN cliente
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        ///<param name="IdClient"></param>
        void DeleteClient(int IdClient);


        /// <summary>
        /// Método para listar Clientes
        /// </summary>
        /// <returns>
        ///  List<Client> 
        /// </returns>
        List<Client> ListClient();


        /// <summary>
        /// Método para consultar un cliente
        /// </summary>
        /// <returns>
        ///  List<Product> 
        /// </returns>
        Client GetClientId(int IdClient);
    }
}

