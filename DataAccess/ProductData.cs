﻿using DataAccess.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ProductData : ProductDataInterface
    {
        ExecuteProcedures procedures;
        Dictionary<string, object> parameters;

        public ProductData()
        {
            procedures = new ExecuteProcedures();
            parameters = new Dictionary<string, object>();
        }

        public void CreateAndUpdateProduct(Product product)
        {
            parameters.Add("IdPorduct", product.IdPorduct);
            parameters.Add("Name", product.Name);
            parameters.Add("UnitValue", product.UnitValue);
            parameters.Add("Active", product.Active);
            var data = procedures.Execute(parameters, "SP_CreateUpdateProduct");
        }

        public void DeleteProduct(int IdPorduct)
        {
            parameters.Add("IdPorduct", IdPorduct);
            procedures.Execute(parameters, "SP_DeleteProduct");
        }

        public Product GetProductId(int IdPorduct)
        {
            Product product = new Product();
            parameters.Add("IdPorduct", IdPorduct);

            var datos = procedures.ConsultList(parameters, "SP_GetProduct");
            if (datos != null && datos.Tables.Count > 0 && datos.Tables[0].Rows.Count > 0)
            {

                var item = datos.Tables[0].Rows[0];
                product.IdPorduct = item.Field<int>("IdPorduct");
                product.Name = item.Field<string>("Name");
                product.UnitValue = item.Field<decimal>("UnitValue");
                product.Active = item.Field<bool>("Active");
            }
            return product;
        }

        public List<Product> ListProduct()
        {
            List<Product> products = new List<Product>();

            var datos = procedures.ConsultList(parameters, "SP_ListProducts");
            if (datos != null && datos.Tables.Count > 0 && datos.Tables[0].Rows.Count > 0)
            {

                products = (from item in datos.Tables[0].AsEnumerable()
                                select new Product()
                                {
                                    IdPorduct = item.Field<int>("IdPorduct"),
                                    Name = item.Field<string>("Name"),
                                    UnitValue = item.Field<decimal>("UnitValue"),
                                    Active = item.Field<bool>("Active")

                                }).ToList();

            }
            return products;
        }
    }
}
