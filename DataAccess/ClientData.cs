﻿using DataAccess.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ClientData : ClientDataInterface
    {
        ExecuteProcedures procedures;
        Dictionary<string, object> parameters;

        public ClientData()
        {
            procedures = new ExecuteProcedures();
            parameters = new Dictionary<string, object>();
        }

        public void CreateAndUpdateClient(Client Client)
        {
            parameters.Add("IdClient", Client.IdClient);
            parameters.Add("Cedula", Client.Cedula);
            parameters.Add("Name", Client.Name);
            parameters.Add("LastName", Client.LastName);
            parameters.Add("Phone", Client.Phone);
            parameters.Add("Active", Client.Active);

            var data = procedures.Execute(parameters, "SP_CreateUpdateClient");
        }

        public void DeleteClient(int IdClient)
        {
            parameters.Add("IdClient", IdClient);
            procedures.Execute(parameters, "SP_DeleteClient");
        }

     
        public List<Client> ListClient()
        {
            List<Client> Clients = new List<Client>();

            var datos = procedures.ConsultList(parameters, "SP_ListClients");
            if (datos != null && datos.Tables.Count > 0 && datos.Tables[0].Rows.Count > 0)
            {

                Clients = (from item in datos.Tables[0].AsEnumerable()
                            select new Client()
                            {
                                IdClient = item.Field<int>("IdClient"),
                                Cedula = item.Field<string>("Cedula"),
                                Name = item.Field<string>("Name"),
                                LastName = item.Field<string>("LastName"),
                                Phone = item.Field<string>("Phone"),
                                Active = item.Field<bool>("Active")


            }).ToList();

            }
            return Clients;
        }

        public Client GetClientId(int IdClient)
        {
            Client client = new Client();
            parameters.Add("IdClient", IdClient);

            var datos = procedures.ConsultList(parameters, "SP_GetClient");
            if (datos != null && datos.Tables.Count > 0 && datos.Tables[0].Rows.Count > 0)
            {

                var item = datos.Tables[0].Rows[0];
                client.IdClient = item.Field<int>("IdClient");
                client.Cedula = item.Field<string>("Cedula");
                client.Name = item.Field<string>("Name");
                client.LastName = item.Field<string>("LastName");
                client.Phone = item.Field<string>("Phone");
                client.Active = item.Field<bool>("Active");

        }
            return client;
        }

    }
}
