﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace DataAccess
{
    class ExecuteProcedures
    {
        SqlConnection Conn = null;
        SqlCommand cmdcommand = null;
        SqlDataAdapter Adaptador = null;
        DataSet Datos = null;

        public ExecuteProcedures()
        {
            if (Conn == null)
                Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

        }

        public string Execute(Dictionary<string, object> Parametros, string procedimiento)
        {
            string result = "";
            try
            {
                SqlCommand cmdcommand = new SqlCommand(procedimiento, Conn);
                cmdcommand.CommandType = CommandType.StoredProcedure;

                foreach (var item in Parametros)
                {
                    SqlParameter parametro = new SqlParameter(item.Key, item.Value);
                    cmdcommand.Parameters.Add(parametro);
                }

                Conn.Open();
                result = cmdcommand.ExecuteScalar().ToString();
                return result;
            }
            catch (Exception ex)
            {
                return result = "";
            }
            finally
            {
                Conn.Close();
            }
        }

        public DataSet ConsultList(Dictionary<string, object> Parametros, string procedimiento)
        {
            try
            {
                cmdcommand = new SqlCommand();
                cmdcommand.CommandType = System.Data.CommandType.StoredProcedure;
                cmdcommand.CommandText = procedimiento;
                cmdcommand.Connection = Conn;
                foreach (var item in Parametros)
                {
                    SqlParameter parametro = new SqlParameter(item.Key, item.Value);
                    cmdcommand.Parameters.Add(parametro);
                }
                Conn.Open();
                Adaptador = new SqlDataAdapter(cmdcommand);
                Datos = new DataSet();
                Adaptador.Fill(Datos);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (Conn.State == System.Data.ConnectionState.Open)
                    Conn.Close();
                cmdcommand.Dispose();
                Adaptador.Dispose();
            }
            return Datos;
        }



    }
}
