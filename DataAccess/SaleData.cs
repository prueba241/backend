﻿using DataAccess.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class SaleData : SaleDataInterface
    {
        ExecuteProcedures procedures;
        Dictionary<string, object> parameters;

        public SaleData()
        {
            procedures = new ExecuteProcedures();
            parameters = new Dictionary<string, object>();
        }

        public void CreateAndUpdateSale(Sale sale)
        {
            parameters = new Dictionary<string, object>();
            parameters.Add("IdSale", sale.IdSale);
            parameters.Add("IdProduct", sale.IdProduct);
            parameters.Add("IdClient", sale.IdClient);
            parameters.Add("Amount", sale.Amount);
            parameters.Add("UnitValue", sale.UnitValue);
            parameters.Add("TotalValue", sale.TotalValue);
            var data = procedures.Execute(parameters, "SP_CreateUpdatesale");
        }

        public List<SaleDetail> ListSales()
        {
            List<SaleDetail> sales = new List<SaleDetail>();

            var datos = procedures.ConsultList(parameters, "SP_Listsales");
            if (datos != null && datos.Tables.Count > 0 && datos.Tables[0].Rows.Count > 0)
            {

                sales = (from item in datos.Tables[0].AsEnumerable()
                            select new SaleDetail()
                            {
                                IdSale = item.Field<int>("IdSale"),
                                IdClient = item.Field<int>("IdClient"),
                                IdProduct = item.Field<int>("IdProduct"),
                                Amount = item.Field<decimal>("Amount"),
                                UnitValue = item.Field<decimal>("UnitValue"),
                                TotalValue = item.Field<decimal>("TotalValue"),
                                NameClient = item.Field<string>("NameClient"),
                                NameProduct = item.Field<string>("NameProduct"),

                            }).ToList();

            }
            return sales;
        }
    }
}
