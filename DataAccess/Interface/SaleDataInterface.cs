﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
    public interface SaleDataInterface
    {
        /// <summary>
        /// Método para crear una venta
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        ///<param name="Sale"></param>
        void CreateAndUpdateSale(Sale sale);


        /// <summary>
        /// Método para listar ventas
        /// </summary>
        /// <returns>
        ///  List<Product> 
        /// </returns>
        List<SaleDetail> ListSales();

    }
}
