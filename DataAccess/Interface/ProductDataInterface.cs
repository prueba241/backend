﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interface
{
    public interface ProductDataInterface
    {
        /// <summary>
        /// Método para crear un nuevo producto
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        ///<param name="Product"></param>
        void CreateAndUpdateProduct(Product product);


        /// <summary>
        /// Método para ELIMINAR UN producto
        /// </summary>
        /// <returns>
        /// void
        /// </returns>
        ///<param name="IdPorduct"></param>
        void DeleteProduct(int IdPorduct);


        /// <summary>
        /// Método para listar productos
        /// </summary>
        /// <returns>
        ///  List<Product> 
        /// </returns>
        List<Product> ListProduct();

        /// <summary>
        /// Método para consultar un productos
        /// </summary>
        /// <returns>
        ///  List<Product> 
        /// </returns>
        Product GetProductId(int IdPorduct);
    }
}
