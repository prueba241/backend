﻿using Business;
using Business.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/client")]
    public class ClientController : ApiController
    {
        ClientInterface ClientBusiness = new ClientBusiness();

        // GET api/values
        public List<Client> Get()
        {
            return ClientBusiness.ListClient();
        }

        // GET api/values/5
        public Client Get(int id)
        {
            return ClientBusiness.GetClientId(id);
        }

        // POST api/Client
        [HttpPost]
        public IHttpActionResult Post(Client Client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ClientBusiness.CreateAndUpdateClient(Client);
            return StatusCode(HttpStatusCode.Created);
        }

        // PUT api/Client
        [HttpPut]
        public IHttpActionResult Put(Client Client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ClientBusiness.CreateAndUpdateClient(Client);
            return StatusCode(HttpStatusCode.OK);
        }

        // DELETE api/Client/5
        [HttpGet]
        [Route("delete/{idClient}")]
        public IHttpActionResult Delete(int idClient)
        {
            ClientBusiness.DeleteClient(idClient);
            return StatusCode(HttpStatusCode.OK);

        }
    }
}
