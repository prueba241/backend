﻿using Business;
using Business.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/sale")]
    public class SaleController : ApiController
    {
        SaleInterface saleBusiness = new SaleBusiness();

        // GET api/values
        public List<SaleDetail> Get()
        {
            return saleBusiness.ListSales();
        }

        [HttpPost]
        public IHttpActionResult Post(Sale sale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            saleBusiness.CreateAndUpdateSale(sale);
            return StatusCode(HttpStatusCode.Created);
        }

        [HttpPost]
        [Route("saleClient")]
        public IHttpActionResult Post(List<Sale> sales)
        {
            saleBusiness.CreateSaleList(sales);
            
            return StatusCode(HttpStatusCode.Created);
        }

    }
}
