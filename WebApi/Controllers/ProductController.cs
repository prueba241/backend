﻿using Business;
using Business.Interface;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : ApiController
    {
        ProductInterface productBusiness = new ProductBusiness();

        // GET api/values
        public List<Product> Get()
        {
            return productBusiness.ListProduct();
        }

        // GET api/values/5
        public Product Get(int id)
        {
            return productBusiness.GetProductId(id);
        }

        // POST api/product
        [HttpPost]
        public IHttpActionResult Post(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            productBusiness.CreateUpdateProduct(product);

            return StatusCode(HttpStatusCode.Created);
        }

        // PUT api/product
        [HttpPut]
        public IHttpActionResult Put(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            productBusiness.CreateUpdateProduct(product);
            return StatusCode(HttpStatusCode.OK);
        }

        // DELETE api/product/5
        [HttpGet]
        [Route("delete/{idProduct}")]
        public IHttpActionResult Delete(int idProduct)
        {
            productBusiness.DeleteProduct(idProduct);
            return StatusCode(HttpStatusCode.OK);

        }
    }
}
